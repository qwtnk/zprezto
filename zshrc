#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
path=($HOME/bin(N-/) $path)
#eval "$(pyenv virtualenv-init -)"
export PYENV_ROOT=/usr/local/opt/pyenv
path=($PYENV_ROOT/bin(N-/) $path)
eval "$(pyenv init -)"
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

function rm() {
    if [ -d ~/.Trash ]; then
        local DATE=`date "+%y%m%d-%H%M%S"`
        mkdir ~/.Trash/$DATE
        for i in $@; do
            if [ -e $i ]; then
                mv $i ~/.Trash/$DATE/
            fi
         done
    else
         /bin/rm $@
    fi
}
